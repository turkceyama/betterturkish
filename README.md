 # BetterTurkish
[İndir](https://gitlab.com/turkceyama/betterturkish/-/releases)
| [SteamWorkshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2383916847)

Prison Architect Gayri Resmi Türkçe Yama.

## Kurulum
**Not:** Bu bölüm Steam dışı kullanıcılar için hazırlanmıştır.
### Windows

1. Yamayı indirin.
2. Zip dosyasının içindeki klasörü "%localappdata%\Introversion\Prison Architect\mods" adresine atın.

### Linux

1. Yamayı indirin.
2. Zip dosyasının içindeki klasörü "~/.Prison Architect/mods" adresine atın.

### Mac

1. Yamayı indirin.
2. Zip dosyasının içindeki klasörü "~/Library/Application Support/Prison Architect/mods" adresine atın.

## İletişim

### Steam Sayfası
[BetterTurkish](https://steamcommunity.com/sharedfiles/filedetails/?id=2383916847)

## Emeği Geçenler

### DarkBloodKing
(https://steamcommunity.com/id/DarkBlood007/)

### Rozzy
(https://steamcommunity.com/id/rozzyrozzyoglu/)